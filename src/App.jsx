import { Provider } from 'react-redux';
import { store } from "./store/store";
import { Route } from 'react-router-dom';
import { BrowserRouter } from "react-router-dom";
import Navigator from "./components/Navigation";
import InfoMain from './components/InfoMain';
import Profile from './components/Profile';
import Product from './components/Product';
import ProductInfo from './components/ProductInfo';

function App() {
    return (
        <Provider store={store}>
            <BrowserRouter>
                <nav>
                    <Navigator />
                    <Route path='/main' component={InfoMain} />
                    <Route path='/infoAdmin' component={Product} />
                    <Route path='/info' component={ProductInfo} />
                    <Route path='/user' component={Profile} />
                </nav>
            </BrowserRouter>
        </Provider>)
}

export default App;

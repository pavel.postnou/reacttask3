import React, {useState} from 'react';
import style from './Info.module.css';
import {connect} from "react-redux";

function Product(props) {
    const [name, setName] = useState();
    const [description, setDescription] = useState();
    const [price, setPrice] = useState();
    const [id, setId] = useState();
    const [isEdit, setIsEdit] = useState(false);

    const sendNewProduct = () => {
        let product = {
            id: isEdit ? id : props.products.length+1,
            price: price,
            name: name,
            description: description
        };
        if(isEdit) {
            props.editProduct(product);
        } else {
            props.addProduct(product);
        }

        setName('');
        setDescription('');
        setPrice('');
        setIsEdit(false);
    }

    const handleEdit = (product) => {
        setIsEdit(true);
        setPrice(product.price);
        setName(product.name);
        setDescription(product.description);
        setId(product.id);
    }

    const handleDelete = (product) => {
        props.deleteProduct(product.id)
    }

        return (
            <>
             <div className={style.text}>Создать новый продукт </div>
             Название <input value={name} onChange={(e) => setName(e.target.value)}/>
             Описание <input value={description} onChange={(e) => setDescription(e.target.value)}/>
             Цена <input value={price} onChange={(e) => setPrice(e.target.value)}/>
             <button onClick={sendNewProduct}>Отправить</button>
                {
                    props.products.map(product => (
                        <div key={product.id}>
                            <div style={{fontSize:20}}><p>{product.name}</p> <p>{product.description}</p> <p>{product.price}</p></div>
                            <button onClick={() => handleEdit(product)}>Изменить</button>
                            <button onClick={() => handleDelete(product)}>Удалить</button>
                            <p>--------------------------</p>
                        </div>
                    ))
                }
            </>
        )
}

const mapStateToProps = (state) => ({
    products: state.products
});

const mapDispatchToProps = (dispatch) => ({
    addProduct: (data) => {dispatch({type: 'ADD_Product', data})},
    editProduct: (data) => {dispatch({type: 'EDIT_Product', data})},
    deleteProduct: (id) => {dispatch({type: 'DELETE_Product', id})},
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Product);

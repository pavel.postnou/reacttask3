import { NavLink } from "react-router-dom";
import "./Navigation.css"
function Navigation() {
    return (
        <div className='Navi'>
            <NavLink
                to="/main">о нас</NavLink>
            <NavLink
                to="/infoAdmin">изменить</NavLink>
            <NavLink
                to="/info">инфо</NavLink>
            <NavLink
                to="/user">аккаунт</NavLink>
        </div>
    )
}
export default Navigation;
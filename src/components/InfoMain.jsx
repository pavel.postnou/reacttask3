import React,{useEffect} from 'react';
import {connect} from "react-redux";
import {getMain} from "../thunk/main.thunk";


function InfoMain(props) {
  useEffect(() => {
    props.onGetAllMain();
}, [])

   return (
       <>
       <img src={props.main.logo} alt = "здесь могла бы быть ваша реклама"/>
       <h1>{props.main.title}</h1>
       <h2>{props.main.text}</h2>
       </>
   )
}

const mapStateToProps = (state) => ({
    main: state.main
});

const mapDispatchToProps = dispatch => ({
  onGetAllMain: () => {dispatch(getMain())}
  });

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(InfoMain);

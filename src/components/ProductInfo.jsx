import React, {useState} from 'react';
import style from './Info.module.css';
import {connect} from "react-redux";

function ProductInfo(props) {
    const [name, setName] = useState();

    const handleFind = (name) => {
        props.findProduct(name)
    }

        return (
            <>
             <div className={style.text}>Найти продукт</div>
             Название <input value={name} onChange={(e) => setName(e.target.value)}/>
             <button onClick={() => handleFind(name)}>Найти</button>
                {
                    props.products.map(product => (
                        <div key={product.id}>
                             <div style={{fontSize:20}}><p>{product.name}</p><p>{product.description}</p><p>{product.price}</p><p>--------------------------</p></div>
                        </div>
                    ))
                }
            </>
        )
}

const mapStateToProps = (state) => ({
    products: state.products
});

const mapDispatchToProps = (dispatch) => ({
    findProduct: (name) => {dispatch({type: 'FIND_Product', name})}
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductInfo);

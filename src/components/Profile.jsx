import React, {useEffect, useState} from 'react';
import style from './Info.module.css';
import {connect} from "react-redux";

function Profile(props) {
    const [name, setName] = useState();
    const [surname, setSurname] = useState();
    const [card, setCard] = useState();
    const [isEdit, setIsEdit] = useState(false);

    useEffect(() => {
    }, [surname, name])

    const sendNewUser = () => {
        let user = {
            card: isEdit ? card : props.users.length+1,
            name: name,
            surname: surname
        };

        if(isEdit) {
            props.editUser(user);
        } else {
            props.addUser(user);
        }

        setName('');
        setSurname('');
        setIsEdit(false);
    }

    const handleEdit = (user) => {
        setIsEdit(true);
        setCard(user.card);
        setName(user.name);
        setSurname(user.surname);
    }

    const handleDelete = (user) => {
        props.deleteUser(user.card)
    }

        return (
            <>
             <div className={style.text}>Создать нового пользователя</div>
             Имя <input value={name} onChange={(e) => setName(e.target.value)}/>
             Фамилия <input value={surname} onChange={(e) => setSurname(e.target.value)}/>
             <button onClick={sendNewUser}>Отправить</button>
                {
                    props.users.map(user => (
                        <div key={user.card}>
                            <div style={{fontSize:40}}>{user.name} {user.surname}</div>
                            <button onClick={() => handleEdit(user)}>Изменить</button>
                            <button onClick={() => handleDelete(user)}>Удалить</button>
                        </div>
                    ))
                }
            </>
        )
}

const mapStateToProps = (state) => ({
    users: state.users
});

const mapDispatchToProps = (dispatch) => ({
    addUser: (data) => {dispatch({type: 'ADD_USER', data})},
    editUser: (data) => {dispatch({type: 'EDIT_USER', data})},
    deleteUser: (card) => {dispatch({type: 'DELETE_USER', card})},
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Profile);

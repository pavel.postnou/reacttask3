const defaultState = [
    {name:'Paul', surname:'Man', card:1},
    {name:'Jack', surname:'Wolf', card:2},
    {name:'Bob', surname:'Torn', card:3}
];

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'ADD_USER':
            return [...state, action.data];
        case 'DELETE_USER':
            return state.filter((el) => el.card !== action.card)
        case 'EDIT_USER':
            let newState = [];
            state.forEach((el) => {
                if(el.card === action.data.card) {
                    newState.push(action.data)
                } else {
                    newState.push(el)
                }});
            return newState;
        default:
            return state;
    }
};

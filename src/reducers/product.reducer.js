const defaultState = [
    { id: 1, name: 'Samsung', description: 'Best tv ever', price: 345 },
    { id: 2, name: 'Sony', description: 'Almost Best tv ever', price: 320 },
    { id: 3, name: 'Philips', description: 'not bad', price: 300 },
];

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'ADD_Product':
            return [...state, action.data];
        case 'DELETE_Product':
            return state.filter((el) => el.id !== action.id)
        case 'FIND_Product':
            console.log(action.name)
            if (action.name!==""&&action.name!==undefined)
            return state.filter((el) => el.name.toLowerCase() === action.name.toLowerCase())
            return state
        case 'EDIT_Product':
            let newState = [];
            state.forEach((el) => {
                if (el.id === action.data.id) {
                    newState.push(action.data)
                } else {
                    newState.push(el)
                }
            });
            return newState;
        default:
            return state;
    }
};

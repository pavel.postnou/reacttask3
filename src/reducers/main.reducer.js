const defaultState = {
    logo:"https://static6.depositphotos.com/1035653/579/i/600/depositphotos_5792529-stock-photo-vintage-room-background.jpg",
    title:"TVSHOP",
    text:"Не наш магазин электроники с доставкой является лидером на белорусском рынке, поскольку предлагает довольно выгодные условия для своих клиентов. Если Вы давно ищете необходимый вид электроники или цифровой техники — обратите внимание на каталог нашего сайта. Покупая у нас, вы получаете прекрасную возможность сэкономить на качественной покупке круглую сумму!"
};

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'ADD_MAIN':
            return action.data
        default:
            return state;
    }
};

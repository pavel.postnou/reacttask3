import {getMainQuery} from "../services/main.service";
import {fetchMainSuccess} from "../actionCreators/main.action";

export const getMain = () => (dispatch) => {
    getMainQuery()
        .then((response) => response.json())
        .then((data) => {
            dispatch(fetchMainSuccess(data));
        })
        .catch((err) => {
            console.log(err)
        })
};

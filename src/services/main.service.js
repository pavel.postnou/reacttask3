export const getMainQuery = () => {
    return fetch("https://jsonplaceholder.typicode.com/posts/1", {
        method: 'PUT',
        body: JSON.stringify({
            logo: "https://static6.depositphotos.com/1035653/579/i/600/depositphotos_5792529-stock-photo-vintage-room-background.jpg",
            title:"TVSHOP",
            text:"Наш магазин электроники с доставкой является лидером на белорусском рынке, поскольку предлагает довольно выгодные условия для своих клиентов. Если Вы давно ищете необходимый вид электроники или цифровой техники — обратите внимание на каталог нашего сайта. Покупая у нас, вы получаете прекрасную возможность сэкономить на качественной покупке круглую сумму!"
          }),
          headers: {
            'Content-type': 'application/json; charset=UTF-8',
          },
    })
}

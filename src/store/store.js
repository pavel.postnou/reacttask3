import {createStore, combineReducers, applyMiddleware} from "redux";
import usersReducer from '../reducers/user.reducer';
import mainReducer from "../reducers/main.reducer";
import productReducer from "../reducers/product.reducer";
import thunk from 'redux-thunk';

export const rootReducer = combineReducers({
    users: usersReducer,
    main: mainReducer,
    products: productReducer
});

export const store = createStore(rootReducer, applyMiddleware(thunk));
